# Copyright 2015 Thomas G. Anderson
# Distributed under the terms of the GNU General Public License v2

MY_PN="${PN}-ng"

require github [ user=opencollab project=${MY_PN} ] \
    autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

SUMMARY="A collection of Fortran77 subroutines designed to solve large scale
eigenvalue problems"
DESCRIPTION="
Important Features:

* Reverse Communication Interface.
* Single and Double Precision Real Arithmetic Versions for Symmetric,
  Non-symmetric, Standard or Generalized Problems.
* Single and Double Precision Complex Arithmetic Versions for Standard or
  Generalized Problems.
* Routines for Banded Matrices - Standard or Generalized Problems.
* Routines for The Singular Value Decomposition.
* Example driver routines that may be used as templates to implement numerous
  Shift-Invert strategies for all problem types, data types and precision.
"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="mpi"

DEPENDENCIES="
    build+run:
        sys-libs/libgfortran:=
        virtual/blas
        virtual/lapack
        mpi? ( sys-cluster/openmpi[fortran] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-icb
    --disable-icb-exmm
    --disable-static
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( mpi )

