# Copyright 2011 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PNV=${PNV/_p/-patch}
WORK="${WORKBASE}"/${MY_PNV}

SUMMARY="Library for storing and managing data"
HOMEPAGE="https://portal.hdfgroup.org/display/support"
DOWNLOADS="https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-$(ever range 1-2)/${MY_PNV}/src/${MY_PNV}.tar.bz2"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/${PN^^}+${PV}"
UPSTREAM_RELEASE_NOTES="https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-$(ever range 1-2)/${MY_PNV}/src/${MY_PNV}-RELEASE.txt"

LICENCES="${PN}"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    doc
    fortran [[ description = [ Build fortran libraries ] ]]
    mpi
"

DEPENDENCIES="
    build:
        doc? ( app-doc/doxygen )
        fortran? ( sys-libs/libgfortran:* )
    build+run:
        mpi? ( sys-cluster/openmpi[fortran=] )
        sys-libs/zlib
"

DEFAULT_SRC_TEST_PARAMS=( TIME=echo )

src_configure() {
    # MPI is incompatible with both shared libraries and C++ bindings
    # however some may still need it
    local myconf=(
        --enable-build-mode=production
        --enable-deprecated-symbols
        # Disable high-level tools, avoids three security issues with gif tool
        # CVE-2018-17433, CVE-2018-17436, CVE-2020-10809
        --enable-hltools=no
        --enable-internal-debug=none
        --enable-optimization=none
        --enable-preadwrite
        --enable-profiling=no
        --enable-symbols=no
        --enable-tools
        --disable-asserts
        --disable-codestack
        --disable-developer-warnings
        --disable-instrument
        --disable-java
        --disable-map-api
        --disable-ros3-vfd
        --disable-sanitize-checks
        --disable-trace
        --without-libhdfs
        $(option_enable doc doxygen)
        $(option_enable fortran)
        $(option_enable mpi parallel)
        $(option_enable mpi static)
        $(option_enable !mpi cxx)
        $(option_enable !mpi shared)
        $(expecting_tests --enable-tests --disable-tests)
    )

    if option mpi; then
        CC=mpicc
        CXX=mpic++
        FC=mpif77
        CFLAGS="${CFLAGS} -fPIC"
        CXXFLAGS="$${CXXFLAGS} -fPIC"
        FFLAGS="${FFLAGS} -fPIC"
    fi

    CC="${CC}" CXX="${CXX}" FC="${FC}" CFLAGS="${CFLAGS}" CXXFLAGS="${CXXFLAGS}" FFLAGS="${FFLAGS}" \
        econf "${myconf[@]}"
}

src_compile() {
    default

    option doc && emake doxygen
}

src_install() {
    default

    if option doc ; then
        edo pushd hdf5lib_docs
        dodoc -r html
        edo popd
    fi
}

