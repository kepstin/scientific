# Copyright 2015 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN=${PN^^}

export_exlib_phases src_unpack src_prepare src_compile src_test src_install

SUMMARY="A library for generating IUPAC International Chemical Identifiers"
DESCRIPTION="
InChI is an acronym for IUPAC International Chemical Identifier. It is a string
of characters capable of uniquely representing a chemical substance and serving
as its unique digital ‘signature’. It is derived solely from a structural
representation of that substance in a way designed to be independent of the way
that the structure was drawn. A single compound will always produce the same
identifier.
In one sentence: InChI provides a precise, robust, IUPAC approved structure-
derived tag for a chemical substance."

HOMEPAGE="https://iupac.org/who-we-are/divisions/division-details/inchi/"
DOWNLOADS="
    https://www.inchi-trust.org/download/$(ever delete_all)/${MY_PN}-1-SRC.zip -> ${PNV}-src.zip
    https://www.inchi-trust.org/download/$(ever delete_all)/${MY_PN}-1-TEST.zip -> ${PNV}-test.zip
    doc? ( https://www.inchi-trust.org/download/$(ever delete_all)/${MY_PN}-1-DOC.zip -> ${PNV}-doc.zip )
"

LICENCES="IUPAC-InChI-Trust-1.0"
SLOT="0"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        app-arch/unzip
    build+run:
        !sci-chemistry/openbabel[<2.3.2] [[
            description = [ openbabel[<2.3.2] bundled this package ]
            resolution = uninstall-blocked-after
        ]]
"

UPSTREAM_RELEASE_NOTES="http://www.inchi-trust.org/download/$(ever delete_all)/RelNotes.pdf"

WORK=${WORKBASE}/${MY_PN}-1-SRC

inchi_src_unpack() {
    default

    edo pushd ${MY_PN}-1-TEST/test
    edo unzip -qoa test-datasets.zip
    edo unzip -qoa test-results.zip
    edo popd
}

inchi_src_prepare() {
    default

    # It seems to be a Windows batch(?) file, let's make it useful for us
    edo sed -e "/REM.*/s/^/#/" \
        -e "s:inchi-1.exe:../../${MY_PN}-1-SRC/INCHI_EXE/bin/Linux/inchi-1:" \
        -i ../${MY_PN}-1-TEST/test/inchify_InChI_TestSet.cmd
}

inchi_src_compile() {
    local common_params=(
        C_COMPILER=$(exhost --build)-cc
        CPP_COMPILER=$(exhost --build)-c++
        LINKER="$(exhost --build)-c++ -s"
        LINKER_OPTIONS="${LDFLAGS}"
        CREATE_MAIN=
        SHARED_LINK="$(exhost --build)-cc"
        SHARED_LINK_PARM="${LDFLAGS} -shared "
        ISLINUX=1
    )

    local lib_params=(
        C_OPTIONS="${CFLAGS} \${P_INCL} -ansi -DCOMPILE_ANSI_ONLY -fPIC -c -DTARGET_API_LIB -D_LIB "
        CPP_OPTIONS="${CXXFLAGS} \${P_INCL} -ansi -c -DTARGET_API_LIB -D_LIB "
    )

    local bin_params=(
        C_OPTIONS="${CFLAGS} \${P_INCL} -ansi -DCOMPILE_ANSI_ONLY -fPIC -c -DTARGET_EXE_STANDALONE "
        CPP_OPTIONS="${CXXFLAGS} \${P_INCL} -ansi -c -DTARGET_EXE_STANDALONE "
    )

    emake -C INCHI_API/libinchi/gcc "${common_params[@]}" "${lib_params[@]}"

    emake -C INCHI_EXE/inchi-1/gcc "${common_params[@]}" "${bin_params[@]}"
}

inchi_src_test() {
    edo pushd ${WORKBASE}/${MY_PN}-1-TEST/test
    edo sh ./inchify_InChI_TestSet.cmd
}

inchi_src_install() {
    # There's no install target
    dobin INCHI_EXE/bin/Linux/inchi-1

    edo mkdir -p "${IMAGE}"/usr/$(exhost --target)/include/${PN}
    insinto /usr/$(exhost --target)/include/${PN}
    doins INCHI_BASE/src/inchi_api.h

    edo pushd INCHI_API/bin/Linux
    dolib.so libinchi.so.*
    edo popd

    emagicdocs
    option doc && dodoc "${WORKBASE}"/INCHI-1-DOC/*
}

